package pl.szer.szerowniamessages.Entity;

public interface Links {
    String BASE_URL = "http://localhost:8080/api/";
    String AD_NAME_LINK = "rentalAd/name/";
    String CLAIM_NAME_LINK = "claims/name/";
    String TICKET_NAME_LINK = "tickets/name/";
}
