package pl.szer.szerowniamessages.Entity;

import java.time.LocalDateTime;

public class Event {

    private String threadId;
    private String authorId;
    private String receiverId;
    private String messageSender;
    private String lastMessage;
    private String subject;
    private LocalDateTime lastMessageTime;

    public Event(Thread t, String messageSender) {
        threadId =t.getId();
        authorId = t.getAuthorId();
        receiverId = t.getReceiverId();
        lastMessage = t.getLastMessage();
        lastMessageTime = t.getLastMessageTime();
        subject = t.getSubject();
        this.messageSender = messageSender;
    }

    public Event(){}

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessageSender() {
        return messageSender;
    }

    public void setMessageSender(String messageSender) {
        this.messageSender = messageSender;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public LocalDateTime getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(LocalDateTime lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
