package pl.szer.szerowniamessages.Entity;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

public class Thread {
    private String id;
    private String authorId;
    private String receiverId;
    private String subject;
    private Long adId;
    private Long ticketId;
    private Long claimId;
    private Flux<Message> messages = Flux.empty();
    private LocalDateTime lastMessageTime;

    public Thread() {
    }

    public Thread(String authorId, String receiverId, String subject, Long adId, Long ticketId, Long claimId) {
        this.authorId = authorId;
        this.receiverId = receiverId;
        this.subject = subject;
        this.adId = adId;
        this.ticketId = ticketId;
        this.claimId = claimId;
    }

    public static Thread fromAd(String authorId, String receiverId, String subject, Long adId) {
        return new Thread(authorId, receiverId, "Ogłoszenie: " + subject, adId, 0L, 0L);
    }

    public static Thread fromClaim(String authorId, String receiverId, String subject, Long claimId) {
        return new Thread(authorId, receiverId, "Szkoda: " + subject, 0L, 0L, claimId);
    }

    public static Thread fromTicket(String authorId, String receiverId, String subject, Long ticketId) {
        return new Thread(authorId, receiverId, "Zgłoszenie: " + subject, 0L, ticketId, 0L);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public Long getAdId() {
        return adId;
    }

    public void setAdId(Long adId) {
        this.adId = adId;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public Long getClaimId() {
        return claimId;
    }

    public void setClaimId(Long claimId) {
        this.claimId = claimId;
    }

    public Flux<Message> getMessages() {
        return messages;
    }

    public Flux<Message> setMessages(Flux<Message> messages) {
        this.messages = messages;
        return this.messages;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLastMessage() {
        Message message = messages.blockLast();
        if (message == null) return "EMPTY";

        return message.getMessage();
    }

    public Long getUnreaded() {
        return messages.filter(message -> !message.isSeen()).count().block();
    }

    public void addMessage(Message message) {
        messages = Flux.concat(messages, Mono.just(message));
    }

    public void setLastMessageTime(LocalDateTime lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public LocalDateTime getLastMessageTime() {
        return lastMessageTime;
    }
}
