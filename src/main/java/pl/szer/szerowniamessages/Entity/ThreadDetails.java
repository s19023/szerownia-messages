package pl.szer.szerowniamessages.Entity;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Data
public class ThreadDetails {

    private final boolean isAboutClaim;
    private final boolean isAboutAd;
    private final String receiver;
    private final Long val;

    public ThreadDetails(String request) {
        if (!containsKey(request)) throw new BadRequest();

        String[] split = request.split("A|T|C");

        this.isAboutAd = request.contains("A");
        this.isAboutClaim = request.contains("C");
        this.receiver = split[0];
        this.val = Long.valueOf(split[1]);
    }

    private boolean containsKey(String threadDetails) {
        return threadDetails.contains("A") || threadDetails.contains("T") || threadDetails.contains("C");
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "cant process your request")
    private static class BadRequest extends RuntimeException {
    }
}
