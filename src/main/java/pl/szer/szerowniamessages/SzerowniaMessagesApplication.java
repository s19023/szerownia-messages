package pl.szer.szerowniamessages;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

import static pl.szer.szerowniamessages.Entity.Links.BASE_URL;

@SpringBootApplication
public class SzerowniaMessagesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SzerowniaMessagesApplication.class, args);
    }

    @Bean
    public WebClient http(WebClient.Builder builder) {
        return builder
                .baseUrl(BASE_URL)
                .build();
    }
}
