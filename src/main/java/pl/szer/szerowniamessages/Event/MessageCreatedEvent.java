package pl.szer.szerowniamessages.Event;

import pl.szer.szerowniamessages.Entity.Event;
import org.springframework.context.ApplicationEvent;

public class MessageCreatedEvent extends ApplicationEvent {
    public MessageCreatedEvent(Event e) {
        super(e);
    }
}
